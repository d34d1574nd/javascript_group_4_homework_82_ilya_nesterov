const mongoose = require('mongoose');
const config = require('./config');

const Artist = require('./models/Artist');
const Album = require('./models/Album');
const Track = require('./models/Track');

const run = async () => {
  await mongoose.connect(config.dbUrl, config.mongoOptions);

  const connection = mongoose.connection;

  const collections = await connection.db.collections();

  for ( let collection of collections) {
    await collection.drop();
  }

  const [linkin, seconds, papaRoach] = await Artist.create(
    {name: 'Linkin Park', photo: 'linkinpark.jpeg', information: 'Group my love'},
    {name: '30 seconds to mars', photo: '30seconds.jpeg', information: 'My second love group'},
    {name: "Papa Roach", photo: "paparoach.jpeg", information: "My love group"}
  );

  const [hibrid, lie, war, metamorphos] = await Album.create(
    {
      titleAlbum: "Hibrid Theory",
      artist: linkin._id,
      year: 2000,
      photoAlbum: 'hibridLP.jpeg'
    },
    {
      titleAlbum: "A beautiful Lie",
      artist: seconds._id,
      year: 2005,
      photoAlbum: "abeatifullie30.jpeg"
    },
    {
      titleAlbum: "This is war",
      artist: seconds._id,
      year: 2009,
      photoAlbum: "thisisWar30.jpeg"
    },
    {
      titleAlbum: "Metamorphosis",
      artist: papaRoach._id,
      year: 2009,
      photoAlbum: "Metamorphosis.jpeg"
    }
  );

  await Track.create(
    {
      titleSong: "in the end",
      album: hibrid._id,
      duration: 211.6
    },
    {
      titleSong: 'One step closer',
      album: hibrid._id,
      duration: 141.6
    },
    {
      titleSong: 'PaperCut',
      album: hibrid._id,
      duration: 183
    },
    {
      titleSong: 'This is war',
      album: war._id,
      duration: 316.2
    },
    {
      titleSong: 'Kings and Queens',
      album: war._id,
      duration: 328.2
    },
    {
      titleSong: 'Hurricane',
      album: war._id,
      duration: 367.2
    },
    {
      titleSong: 'ATTACK',
      album: lie._id,
      duration: 185.4
    },
    {
      titleSong: 'A Beautiful Lie',
      album: lie._id,
      duration: 243
    },
    {
      titleSong: 'The Kill',
      album: lie._id,
      duration: 210.6
    },
    {
      titleSong: 'From yesterday',
      album: lie._id,
      duration: 244.2
    },
    {
      titleSong: 'Live This Down',
      album: metamorphos._id,
      duration: 201
    }
  );

  return connection.close();
};

run().catch(error => {
  console.log(error)
});